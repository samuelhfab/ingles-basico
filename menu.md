## MENU

[Classes 01 | Names](https://gitlab.com/samuelhfab/ingles-basico/-/blob/main/Classes/classes01.md)  

[Classes 02 | Introductions](https://gitlab.com/samuelhfab/ingles-basico/-/blob/main/Classes/classes02.md)  

[Classes 03 | Greetings](https://gitlab.com/samuelhfab/ingles-basico/-/blob/main/Classes/classes03.md)  

[Classes 04 | Greetings II](https://gitlab.com/samuelhfab/ingles-basico/-/blob/main/Classes/classes04.md)  

[Classes 05 | Occupations](https://gitlab.com/samuelhfab/ingles-basico/-/blob/main/Classes/classes05.md?ref_type=heads)

[Classes 06 | Occupations 2](https://gitlab.com/samuelhfab/ingles-basico/-/blob/main/Classes/classes06.md?ref_type=heads)

[Classes 07 | Countries](https://gitlab.com/samuelhfab/ingles-basico/-/blob/main/Classes/classes07.md?ref_type=heads)