# NAMES

## What's your name?

**Common writing**

_What is your name?  
My name is Samuel._

**Common speech**
<!-- Uótziú neime -->
_What's your name?  
It's Samuel_  

---
### Samuel Henrique Fabricio da Silva
---
**Primeiro nome** | **Centro do nome** | **Último nome**
--------------|--------------|----------
First name  <!-- fanst neime -->  | Middle name <!-- mirow neime --> | Last name <!-- lest neime -->
Christian name |              | Family name 
Forename <!-- For neime -->     |              | Surname <!-- sarn neime -->

**Nome completo**  
Full name <!-- Fuu neime -->| Complete name <!-- Camplit neime -->

### Name division

First name | Middle name | Last name
:---------:|:-----------:|:---------:
Samuel|Henrique Fabricio|Silva

---

[KULTIVI](https://kultivi.com/)

[First class](C:/Users/samue/OneDrive/Documentos/Ingl%C3%AAs/3be10a49832cd373_CLASS1-INTRODUOENAMES.pdf)

[Voltar](https://gitlab.com/samuelhfab/ingles-basico/-/blob/main/menu.md)