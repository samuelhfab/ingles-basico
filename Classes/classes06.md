# Occupations 2

What's your occupations?
I am a waiter.
I am an actor.

Use "an" antes de palavra iniciada com vogal.

What do you do?
I act in movies and plays.

## Verbs

**English** | **Translator**
------------|--------------
act | agir, atuar
prepare | preparar
cook | cozinhar
treat | tratar
help | ajudar
protect | proteger
answer | responder
talk | conversar
study | estudar
serve | servir
buy | comprar
sell | vender
rent | alugar

[Voltar](https://gitlab.com/samuelhfab/ingles-basico/-/blob/main/menu.md) 