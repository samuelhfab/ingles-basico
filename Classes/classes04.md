# Greetings  <!--guinrins-->

**How are you?**  

**Eu estou** | **Muito** | **Bem** | **+/-** | **Mal**  
---|---|---|---|---  
I am | Very | Well | So so | Not very well
I'm | Pretty | Fine |
¬  | Quite | Good |

**Thanks** or **Thank you.**  

**And you?**  
* How about you?  <!--abar.iu-->
* What about you?  
* How are you?  

How are you?  
Fine, thanks. And you?  

How are you doing?  
I am pretty fine, thank you. How about you?  

How are things?  
Not bad, thanks. How are you?  

**!!!SLANG!!!** <!--Gíria--> 

A- What's up?  <!--Eai? Qualé? /uazáp/-->  
B- What's up!  
B- Not much!  <!--Nada demais-->  

**DIALOGUE**  

A – HELLO, JOHN! GOOD EVENING!  
B – HELLO CHRIS, GOOD EVENING. HOW ARE YOU DOING?  
A – I’M FINE, THANKS. AND YOU?  
B – NOT BAD, THANKS.  
B – CHRIS, LET ME INTRODUCE YOU TO MY FRIEND BILL.  
B – BILL, THIS IS CHRIS, CHRIS THIS IS BILL.  
A – IT’S A PLEASURE TO MEET YOU BILL.  
C- GLAD TO MEET YOU CHRIS.  
A – PEOPLE, I AM SORRY, BUT I HAVE TO GO NOW. GOOD BYE, BILL. SEE YOU TOMORROW, JOHN.  
B – BYE BYE, TAKE CARE!  
C – SEE YOU SOON. BYE!  

[Voltar](https://gitlab.com/samuelhfab/ingles-basico/-/blob/main/menu.md)   