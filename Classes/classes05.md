# Occupations <!--oquiupeicham--> & Profession

**He** | **She**
--------------|--------------
He is an actor<!--ector--> | She is an Actress<!--ectris-->
He is an architect<!--orquitect--> |
He is an astronaut<!--estronaut--> |
He is a baker<!--beiker--> | 
He is a mason<!--meisan--> |
He is a butcher<!--bótier--> |
 | She is a cashier<!--quéchir--> 
He is a cook |
He is a dentist<!--dentêst--> |
He is a doctor |
He is an electrician<!--electrishen--> |
He is an engineer<!--endinier-->  |
He is a fireman<!--faiêrmen--> |
He is a gardener<!--gardnêr--> |
He is a lawyer<!--loiêr--> |
He is a mechanic<!--mequênic--> |
 | She is a nurse<!--norse-->
He is a painter<!--peinchtêr--> |
He is a plumber<!--plamêr--> |
He is a police officer |
He is a realtor |
 | She is a secretary<!--secruétérê-->/receptionist<!--uesépchonest-->
He is a singer |
He is a soccer player |
 | She is a student 
 | She is a teacher
He is a waiter | She is a waitress 

[Voltar](https://gitlab.com/samuelhfab/ingles-basico/-/blob/main/menu.md)   