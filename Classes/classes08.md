# Alphabet

A - êi
B - bí
C - cí
D - dí
E - í
F - éf
G - dgí 
H - éidj
I - ái
J - djêi
K - kêi
L - él
M - êm
N - ên
O - ôu
P - pí
Q - kíu
R - ár
S - és
T - tí
U - iuú
V - ví
W - dábou.iú
X - eks
Y - uái
Z - zí

[Voltar](https://gitlab.com/samuelhfab/ingles-basico/-/blob/main/menu.md) 