# Greetings  <!--guinrins-->

* Informal Greetings  
Hi  
Hello  
---  
Good Morning  
Good Afeternoon  <!-- Éfeternum-->
Good Evening  <!-- Iviring-->

* Saying good bye  

Good Night  
Bye  
Bye bye  
See you  <!-- Te vejo-->
See you Later  <!-- Leirer | Mais tarde-->
So long  <!-- Até breve-->
Take Care  <!-- Têiker-->
See you around  <!--Vejo você por ai-->

**Useful phrases**    
I'm sorry, but i have to go now. <!-- baraírevê tugol nau-->  
_"Desculpe, mas eu tenho que ir agora."_

* Formal Greetings  

_"Como está você?"_
How are you?   <!-- Rau.er.iu-->  
How are you doing?   <!-- Dúin-->
How are things with you?  <!--Tengs.wít.u-->
"Como estão as coisas com você?"  
How are things?  
"Como estão as coisas?"  
How's life treating you?  <!-- Halls.life.tchuirêm.iu-->
"Como a vida está tratando você?"  

A: How are you?  
B: I'm pretty <!--Pridê--> fine <!--Faine-->, thanks, and you?
A: I'm fine, thanks.

[Voltar](https://gitlab.com/samuelhfab/ingles-basico/-/blob/main/menu.md)  