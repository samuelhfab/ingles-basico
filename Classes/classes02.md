# Introductions <!-- entrudóctchan -->  

* **Introducing yourself** <!-- entruducion iusel -->

Hi - Oi <!-- Rái -->  
Hello - Olá <!-- rélou -->  
I am... <!-- Ai ême --> or I'm...  
My name is... or My name's...   

---  

Hi, my name's Mike.  
Hello, my name's Mike.  
Hi, I'm Mike.  
Hello, I'm Mike.  

---

### Nice to meet you! <!-- Nais tuh mí.riu or Nais tchu mí.tchu -->  |  "Muito Prazer!".  

## Examples 
 
Hi, I'm Paul. What's your name?  
My name's Mike.  
Nice to meet you Mike.  
Nice to meet you too Paul.

**OBS**  

Nice to meet you!  
Glad<!-- glédti --> to meet you!  
Pleased<!-- pliz --> to meet you!  
It's a pleasure<!-- êtês plegêr--> to meet you!  

---
* **Introdicing other people.**  

John, this is<!-- dêzes --> Sarah.  
Sarah, this is John.  

## Examples

Samuel: John, this is Sarah.  
Samuel: Sarah, this is John.   
Sarah: Nice to meet you John.  
John: Nice to meet you too Sarah.  

**Useful phrases**  

Let me introduce you to (friend/brother) <!-- Létme intchuducíu tchumai frendi. -->  
_"Deixe-me apresentar a você meu amigo."_  
 
## Examples

A: Hi, Mike! Good Morning.  
B: Hello Bill, good morning.  
A: Let me introduce you to my friend Ted.  
A: Ted this is Mike. Mike this is Ted.  
B: Nice to meet you, Ted.  
C: It's a pleasure to meet you Mike.  

[Voltar](https://gitlab.com/samuelhfab/ingles-basico/-/blob/main/menu.md)  
